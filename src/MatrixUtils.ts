import {Matrix} from "./Matrix";
import {assert} from "./utils";
import {MatrixFactory} from "./MatrixFactory";
import {MatrixDecomposition} from "./MatrixDecomposition";

export class MatrixUtils {
    public static isSquare(matrix: Matrix): boolean {
        return matrix.columnsCount === matrix.rowsCount
    }

    public static isDiagonal(matrix: Matrix): boolean {
        for (let j = 0; j < matrix.rowsCount; j++) {
            for (let i = 0; i < matrix.columnsCount; i++) {
                if (i !== j && matrix.get(i, j) !== 0) return false
            }
        }
        return true
    }

    public static isIdentity(matrix: Matrix): boolean {
        for (let j = 0; j < matrix.rowsCount; j++) {
            for (let i = 0; i < matrix.columnsCount; i++) {
                if (i !== j && matrix.get(i, j) !== 0 || i === j && matrix.get(i, j) !== 1) return false
            }
        }
        return true
    }

    public static isLowerTriangular(matrix: Matrix): boolean {
        for (let j = 0; j < matrix.rowsCount; j++) {
            for (let i = j + 1; i < matrix.columnsCount; i++) {
                if (matrix.get(i, j) !== 0) return false
            }
        }
        return true
    }

    public static isUpperTriangular(matrix: Matrix): boolean {
        for (let j = 0; j < matrix.rowsCount; j++) {
            for (let i = 0; i < j; i++) {
                if (matrix.get(i, j) !== 0) return false
            }
        }
        return true
    }

    public static isTriangular(matrix: Matrix): boolean {
        return MatrixUtils.isLowerTriangular(matrix) || MatrixUtils.isUpperTriangular(matrix)
    }

    public static transpose(matrix: Matrix): Matrix {
        const tmp = new Matrix(matrix.columnsCount, matrix.rowsCount)
        for (let j = 0; j < matrix.rowsCount; j++) {
            for (let i = 0; i < matrix.columnsCount; i++) {
                tmp.set(j, i, matrix.get(i, j))
            }
        }
        return tmp
    }

    public static multiplication(...matrix: Array<Matrix>): Matrix {
        assert(matrix.length > 0) // todo add message
        if (matrix.length === 1) return matrix[0]
        let tmp = matrix[0].clone()
        for (let i = 1; i < matrix.length; i++) {
            // todo add assertion
            const tmp2 = MatrixFactory.zero(tmp.rowsCount, matrix[i].columnsCount)
            for (let y = 0; y < tmp.rowsCount; y++) {
                for (let x = 0; x < matrix[i].columnsCount; x++) {
                    assert(tmp.columnsCount === matrix[i].rowsCount)
                    let sum = 0
                    for (let t = 0; t < tmp.columnsCount; t++) {
                        sum += tmp.get(t, y) * matrix[i].get(x, t)
                    }
                    tmp2.set(x, y, sum)
                }
            }
            tmp = tmp2
        }
        return tmp
    }

    public static sum(...matrix: Array<Matrix>): Matrix {
        assert(matrix.length > 0) // todo add message
        if (matrix.length === 1) return matrix[0]
        const result = matrix[0].clone()
        for (let k = 1; k < matrix.length; k++) {
            assert(result.columnsCount === matrix[k].columnsCount
                && result.rowsCount === matrix[k].rowsCount)
            result.forEachElement((value, i, j) => value + matrix[k].get(i, j))
        }
        return result
    }

    public static scalarMultiplication(matrix: Matrix, scalar: number): Matrix {
        const result = matrix.clone()
        result.forEachElement(value => value * scalar)
        return result
    }

    public static determinant(matrix: Matrix): number {
        assert(MatrixUtils.isSquare(matrix))
        const {L, U} = MatrixDecomposition.LU(matrix)
        let determinantL = 1
        let determinantU = 1
        for (let i = 0; i < matrix.rowsCount; i++) {
            determinantL *= L.get(i, i)
            determinantU *= U.get(i, i)
        }
        return determinantL * determinantU
    }

    public static vectorNorm(matrix: Matrix): number {
        assert(matrix.columnsCount === 1)
        let sum = 0
        matrix.forEachElement(value => {
            sum += Math.pow(value, 2)
        })
        return Math.sqrt(sum)
    }

    public static normalizeVector(matrix: Matrix): Matrix {
        assert(matrix.columnsCount === 1)
        return this.scalarMultiplication(matrix, 1 / this.vectorNorm(matrix))
    }

    public static inverse(matrix: Matrix): Matrix {
        const {L, U} = MatrixDecomposition.LU(matrix)
        return matrix.clone() // todo
    }

    public static GramSchmidt(matrix: Matrix): Matrix {
        function proj(v: Matrix, u: Matrix) {
            assert(v.rowsCount === 1)
            assert(u.rowsCount === 1)
            const t1 = MatrixUtils.multiplication(v, u).get(0, 0)
            const t2 = MatrixUtils.multiplication(u, u).get(0, 0)
            return MatrixUtils.scalarMultiplication(u.clone(), t1 / t2)
        }

        for (let i = 2; i < matrix.rowsCount; i++) {
            // todo
        }

        return matrix
    }
}