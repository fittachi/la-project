import {Matrix} from "./Matrix";
import {assert} from "./utils";

export class MatrixFactory {
    public static zero(rowsCount: number, columnsCount?: number): Matrix {
        return new Matrix(rowsCount, columnsCount)
    }

    public static diagonal(rowsCount: number, columnsCount: number, diagonalValues: Array<number>): Matrix {
        assert(Math.min(rowsCount, columnsCount) >= diagonalValues.length)// todo add message
        const tmp = this.zero(rowsCount, columnsCount)
        for (let index = 0; index < diagonalValues.length; index++) {
            tmp.set(index, index, diagonalValues[index])
        }
        return tmp
    }

    public static identity(rowsCount: number, columnsCount?: number): Matrix {
        const tmp = this.zero(rowsCount, columnsCount)
        for (let index = 0; index < Math.min(rowsCount, columnsCount || rowsCount); index++) {
            tmp.set(index, index, 1)
        }
        return tmp;
    }

    public static vector(values: Array<number>): Matrix {
        const tmp = new Matrix(values.length, 1)
        tmp.data = values
        return tmp
    }

    public static from(values: Array<Array<number>>) {
        const tmp = new Matrix(values.length, values[0].length)
        tmp.data = []
        for (const rowValues of values) {
            assert(rowValues.length === tmp.columnsCount)
            tmp.data = tmp.data.concat(rowValues)
        }
        return tmp
    }
}