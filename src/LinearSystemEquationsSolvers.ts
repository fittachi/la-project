import {Matrix} from "./Matrix";
import {MatrixFactory} from "./MatrixFactory";

export interface JacobiSolverConfig {
    errorThreshold?: number
    itCount?: number
    hint?: Matrix
}

export type GaussSeidelConfig = JacobiSolverConfig

export interface SuccessiveOverRelaxationConfig extends GaussSeidelConfig {
    omega?: number
}


interface SolverCTX {
    errorThreshold: number
    itCount: number
    hint: Matrix
    omega: number
}

function validateConfig(config: SuccessiveOverRelaxationConfig | JacobiSolverConfig, rowsCount: number): SolverCTX {
    let hint: Matrix
    let itCount: number
    let errorThreshold: number
    let omega: number
    if ("omega" in config && config.omega) {
        omega = config.omega
    } else {
        omega = 1.1
    }
    if (config.hint) {
        hint = config.hint
    } else {
        hint = MatrixFactory.zero(rowsCount, 1)
    }
    if (config.itCount) {
        itCount = config.itCount
        errorThreshold = 0
    } else {
        itCount = Infinity
        if (config.errorThreshold) {
            errorThreshold = config.errorThreshold
        } else {
            errorThreshold = 1e-10
        }
    }
    return {hint, itCount, errorThreshold, omega}
}

export class LinearSystemEquationsSolvers {
    public static Jacobi(A: Matrix, B: Matrix, config: JacobiSolverConfig = {}): { Xs: Array<Matrix>, X: Matrix } {
        const ctx = validateConfig(config, B.rowsCount)
        const results: Array<Matrix> = []

        results.push(ctx.hint)
        for (let k = 0; k < ctx.itCount; k++) {
            const Xk = MatrixFactory.zero(B.rowsCount, 1)
            for (let i = 0; i < A.columnsCount; i++) {
                let sum = 0
                for (let j = 0; j < A.rowsCount; j++) {
                    if (i !== j) {
                        sum += A.get(i, j) * results[results.length - 1].get(j, 0)
                    }
                }
                const xi = 1 / A.get(i, i) * (B.get(i, 0) - sum)
                Xk.set(i, 0, xi)
            }
            results.push(Xk)
            if (Math.abs(Xk.get(0, 0) - results[results.length - 2].get(0, 0)) <= ctx.errorThreshold) break
        }
        return {Xs: results, X: results[results.length - 1]}
    }

    public static GaussSeidel(A: Matrix, B: Matrix, config: GaussSeidelConfig = {}): { Xs: Array<Matrix>, X: Matrix } {
        return LinearSystemEquationsSolvers.SuccessiveOverRelaxation(A, B, {...config, omega: 1})
    }

    public static SuccessiveOverRelaxation(A: Matrix, B: Matrix, config: SuccessiveOverRelaxationConfig = {}): { Xs: Array<Matrix>, X: Matrix } {
        const ctx = validateConfig(config, B.rowsCount)
        const results: Array<Matrix> = []

        results.push(ctx.hint)
        for (let k = 0; k < ctx.itCount; k++) {
            const Xk = MatrixFactory.zero(B.rowsCount, 1)
            for (let i = 0; i < A.columnsCount; i++) {
                let sum1 = 0
                for (let j = 0; j < i; j++) {
                    if (i !== j) {
                        sum1 += A.get(i, j) * Xk.get(j, 0)
                    }
                }
                let sum2 = 0
                for (let j = i + 1; j < A.rowsCount; j++) {
                    sum2 += A.get(i, j) * results[results.length - 1].get(j, 0)
                }
                const xi = ctx.omega / A.get(i, i) * (B.get(i, 0) - sum1 - sum2) + (1 - ctx.omega) * results[results.length - 1].get(i, 0)
                Xk.set(i, 0, xi)
            }
            results.push(Xk)
            if (Math.abs(Xk.get(0, 0) - results[results.length - 2].get(0, 0)) <= ctx.errorThreshold) break
        }
        return {Xs: results, X: results[results.length - 1]}
    }
}