import {Matrix} from "./Matrix"
import {assert} from "./utils"
import {MatrixUtils} from "./MatrixUtils"
import {MatrixFactory} from "./MatrixFactory";

function computeMinor(matrix: Matrix, d: number) {
    const result = MatrixFactory.identity(matrix.rowsCount, matrix.columnsCount)
    for (let i = d; i < matrix.rowsCount; i++) {
        for (let j = d; j < matrix.columnsCount; j++) {
            result.set(j, i, matrix.get(j, i))
        }
    }
    return result
}

function extractColumn(matrix: Matrix, c: number) {
    const v = MatrixFactory.zero(matrix.rowsCount, 1)
    for (let j = 0; j < matrix.rowsCount; j++) {
        v.set(0, j, matrix.get(c, j))
    }
    return v
}

function computeHouseholderFactor(v: Matrix) {
    const result = MatrixFactory.identity(v.rowsCount)
    const n = v.rowsCount
    for (let i = 0; i < n; i++) {
        for (let j = 0; j < n; j++) {
            result.set(i, j, result.get(i, j) + -2 * v.get(0, i) * v.get(0, j))
        }
    }
    return result
}

function removeSmallValue(matrix: Matrix, threshold = 1e-10): void {
    matrix.forEachElement(v => Math.abs(v) <= threshold ? 0 : v)
}

export class MatrixDecomposition {

    /**
     * doolittle and crout algorithm
     */
    static LU(matrix: Matrix): { L: Matrix, U: Matrix } {
        assert(MatrixUtils.isSquare(matrix))
        const n = matrix.rowsCount
        const lower = new Matrix(n)
        const upper = new Matrix(n)

        for (let i = 0; i < n; i++) {
            for (let k = i; k < n; k++) {
                let sum = 0
                for (let j = 0; j < i; j++) {
                    sum += (lower.get(j, i) * upper.get(k, j))
                }
                upper.set(k, i, (matrix.get(k, i) - sum))
            }
            for (let k = i; k < n; k++) {
                if (i == k) {
                    lower.set(i, i, 1)
                } else {
                    let sum = 0
                    for (let j = 0; j < i; j++) {
                        sum += (lower.get(j, k) * upper.get(i, j))
                    }
                    lower.set(i, k, (matrix.get(i, k) - sum) / upper.get(i, i))
                }
            }
        }
        return {L: lower, U: upper}
    }


    /**
     * LDMt Decomposition
     */
    static LDMTranspose(matrix: Matrix): { L: Matrix, D: Matrix, Mt: Matrix } {
        const {L, U} = MatrixDecomposition.LU(matrix)
        const n = matrix.rowsCount
        const D = new Matrix(n)
        const Di = new Matrix(n)
        for (let i = 0; i < n; i++) {
            D.set(i, i, U.get(i, i))
            Di.set(i, i, 1 / U.get(i, i))
        }
        const Mt = MatrixUtils.multiplication(Di, U);
        return {L, D, Mt}
    }

    /**
     * Householder algorithm
     */
    static QR(matrix: Matrix): { Q: Matrix, R: Matrix } {
        const m = matrix.rowsCount
        const n = matrix.columnsCount
        const factors: Array<Matrix> = []
        let tmp = matrix.clone()
        let tmp2: Matrix

        for (let k = 0; k < n && k < m - 1; k++) {
            tmp2 = computeMinor(tmp, k)

            const x = extractColumn(tmp2, k)

            let a = MatrixUtils.vectorNorm(x)

            if (matrix.get(k, k) > 0) {
                a = -a
            }

            let e = MatrixFactory.zero(matrix.rowsCount, 1)

            e.set(0, k, 1)

            e = MatrixUtils.sum(x, MatrixUtils.scalarMultiplication(e, a))

            e = MatrixUtils.scalarMultiplication(e, 1 / MatrixUtils.vectorNorm(e))

            factors[k] = computeHouseholderFactor(e)

            tmp = MatrixUtils.multiplication(factors[k], tmp2)
        }

        let Q = factors[0].clone()

        for (let i = 1; i < n && i < m - 1; i++) {
            tmp2 = MatrixUtils.multiplication(factors[i], Q);
            Q = tmp2.clone();
        }

        const R = MatrixUtils.multiplication(Q, matrix)
        Q = MatrixUtils.transpose(Q)
        removeSmallValue(Q)
        removeSmallValue(R)
        return {Q, R}
    }
}