export class Matrix {
    public rowsCount: number
    public columnsCount: number
    public data: Array<number>

    constructor(rowsCount: number, columnsCount?: number) {
        if (!columnsCount) columnsCount = rowsCount
        this.rowsCount = rowsCount
        this.columnsCount = columnsCount
        this.data = Array(columnsCount * rowsCount).fill(0)
    }

    public get(i: number, j: number): number {
        return this.data[j * this.columnsCount + i]
    }

    public set(i: number, j: number, value: number): void {
        this.data[j * this.columnsCount + i] = value
    }

    public equal(matrix: Matrix): boolean {
        if (this.columnsCount === matrix.columnsCount && this.rowsCount === matrix.rowsCount) {
            for (let i = 0; i < this.columnsCount * this.rowsCount; i++) {
                if (this.data[i] !== matrix.data[i]) return false
            }
            return true
        } else return false
    }

    public clone(): Matrix {
        const clone = new Matrix(this.rowsCount, this.columnsCount)
        clone.forEachElement((value, i, j) => this.get(i, j))
        return clone
    }

    public toString(withNextLine = true): string {
        let result = '['
        if (withNextLine) result += '\n'
        for (let j = 0; j < this.rowsCount; j++) {
            if (withNextLine) result += '\t'
            result += '['
            for (let i = 0; i < this.columnsCount; i++) {
                result += this.get(i, j)
                if (i !== this.columnsCount - 1) result += ', '
            }
            result += ']'
            if (j !== this.rowsCount - 1) {
                result += ','
            }
            if (withNextLine) result += '\n'
        }
        result += ']'
        return result
    }

    forEachElement(callback: (value: number, i: number, j: number) => number | void): void {
        for (let j = 0; j < this.rowsCount; j++) {
            for (let i = 0; i < this.columnsCount; i++) {
                const value = this.get(i, j)
                const result = callback(value, i, j)
                this.set(i, j, typeof result === 'undefined' ? value : result)
            }
        }
    }
}
