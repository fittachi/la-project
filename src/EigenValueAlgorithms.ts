import {Matrix} from "./Matrix";
import {MatrixUtils} from "./MatrixUtils";
import {MatrixFactory} from "./MatrixFactory";

export interface scaledPowerConfig {
    errorThreshold?: number
    itCount?: number
    hint?: Matrix
}

export class EigenValueAlgorithms {
    public static scaledPower(A: Matrix, config: scaledPowerConfig = {}): {
        greatestEigenValues: Array<number>,
        greatestEigenVectors: Array<Matrix>
    } {
        let itCount: number
        let errorThreshold: number
        let x: Matrix
        if (config.hint) {
            x = config.hint
        } else {
            x = MatrixFactory.identity(A.rowsCount, 1)
        }
        if (config.itCount) {
            itCount = config.itCount
            errorThreshold = 0
        } else {
            itCount = Infinity
            if (config.errorThreshold) {
                errorThreshold = config.errorThreshold
            } else {
                errorThreshold = 1e-10
            }
        }
        const eigenValues: Array<number> = []
        const eigenVectors: Array<Matrix> = []
        const a = A.clone()
        let y: Matrix
        for (let k = 0; k < itCount; k++) {
            y = MatrixUtils.multiplication(a, x)
            x = MatrixUtils.normalizeVector(y)
            eigenValues.push(MatrixUtils.multiplication(MatrixUtils.transpose(x), a, x).get(0, 0))
            eigenVectors.push(x.clone())
            if (Math.abs(eigenValues[eigenValues.length - 1] - eigenValues[eigenValues.length - 2]) <= errorThreshold) break
        }
        return {greatestEigenValues: eigenValues, greatestEigenVectors: eigenVectors}
    }

    public static shiftedInversePower(A: Matrix, config: scaledPowerConfig = {}) : {
        smallestEigenValues: Array<number>,
        smallestEigenVectors: Array<Matrix>
    }{
        let itCount: number
        let errorThreshold: number
        let x: Matrix
        if (config.hint) {
            x = config.hint
        } else {
            x = MatrixFactory.identity(A.rowsCount, 1)
        }
        if (config.itCount) {
            itCount = config.itCount
            errorThreshold = 0
        } else {
            itCount = Infinity
            if (config.errorThreshold) {
                errorThreshold = config.errorThreshold
            } else {
                errorThreshold = 1e-10
            }
        }
        const eigenValues: Array<number> = []
        const eigenVectors: Array<Matrix> = []
        const a = A.clone()
        const aInv = MatrixUtils.inverse(a)
        let y: Matrix
        for (let k = 0; k < itCount; k++) {
            y = MatrixUtils.multiplication(aInv, x)
            x = MatrixUtils.normalizeVector(y)
            eigenValues.push(MatrixUtils.multiplication(MatrixUtils.transpose(x), a, x).get(0, 0))
            eigenVectors.push(x.clone())
            if (Math.abs(eigenValues[eigenValues.length - 1] - eigenValues[eigenValues.length - 2]) <= errorThreshold) break
        }
        return {
            smallestEigenValues: eigenValues, smallestEigenVectors: eigenVectors
        }
    }
}