export function assert(assertion: boolean, msg?: string): void {
    if (!assertion) throw new Error(msg)
}
